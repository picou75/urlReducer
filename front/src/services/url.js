import validUrl from 'valid-url'
import { postUrl } from '../api/url'

export const isValidUrl = (url, setIsNotificationShown, setError) => {
  if (validUrl.isUri(url) === undefined) {
    setIsNotificationShown(true)
    setError('Veuillez indiquer une url valide !')
    return false
  }
  return true
}

export const postUrlToReduce = async (url, setIsNotificationShown, setError) => {
  const shortUrl = await postUrl(url)
  if (shortUrl === null) {
    setIsNotificationShown(true)
    setError('erreur: vérifiez votre connexion internet')
  }
  return shortUrl
}
