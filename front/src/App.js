import React, { useState } from 'react'
import ListLastUrls from './components/ListLastUrls'
import InputPostUrl from './components/InputPostUrl'
import { isValidUrl, postUrlToReduce } from './services/url'

export default function App () {
  const [url, setUrl] = useState('')
  const [listShortUrls, setListShortUrls] = useState([])

  const [isNotificationShown, setIsNotificationShown] = useState(false)
  const [error, setError] = useState('')

  const handleUrlChange = (url) => setUrl(url)

  const handleUrlPost = async () => {
    setIsNotificationShown(false)
    if (isValidUrl(url, setIsNotificationShown, setError)) {
      const newUrl = await postUrlToReduce(url, setIsNotificationShown, setError)
      if (newUrl !== null) {
        setShortUrl(newUrl)
        setUrl('')
        const isPresent = await listShortUrls.find((url) => url === newUrl)
        if (isPresent === undefined) {
          setListShortUrls([...listShortUrls, newUrl])
        }
      }
    }
  }

  return (
    <div className='bg-gradient-to-r from-purple-500 via-pink-600 to-yellow-500'>
      <div className='grid grid-flow-col grid-cols-2'>
        <div className='pt-5 flex flex-col items-center h-screen'>
          <h1 className='text-opacity-50 font-medium text-5xl text-gray-100'>
            ReductURL
          </h1>
          <div className='pt-5 pl-5'>
            <InputPostUrl url={url} handleUrlChange={handleUrlChange} handlePostUrl={handleUrlPost} />
            {isNotificationShown && (<div className='pl-2 italic text-xs text-gray-200'>{error}</div>)}
          </div>
        </div>
        <ListLastUrls className='' lastUrls={listShortUrls} />
      </div>
    </div>
  )
}
