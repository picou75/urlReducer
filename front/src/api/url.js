import axios from 'axios'
import config from '../config'

const apiUrlReducer = axios.create({
  baseURL: config.api.url + '/url',
  responseType: 'json',
  headers: {
    'Content-Type': 'application/json'
  }
})

export const postUrl = async (url) => {
  return await apiUrlReducer
    .post('/reducer', { url: url })
    .then((res) => {
      return res.data.shortUrl
    })
    .catch((error) => {
      console.log('api url : postUrl [err]: ', error)
      return null
    })
}

export const redirection = async (shortUrl) => {
  return await apiUrlReducer
    .get(`/${shortUrl}`)
    .then(() => {
      return true
    })
    .catch((error) => {
      console.log('api url : redirection [err]: ', error)
      return false
    })
}
