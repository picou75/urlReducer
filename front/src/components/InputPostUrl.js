import React from 'react'

const InputPostUrl = ({ url, handleUrlChange, handlePostUrl }) => {
  const onChange = (e) => handleUrlChange(e.target.value)
  return (
    <>
      <input type='text' className='text-pink-500 p-3 rounded-md shadow-xl shadow-inner focus:outline-none' placeholder='Url...' value={url} onChange={onChange} />
      <button type='button' className='p-3 rounded-md focus:outline-none' onClick={handlePostUrl}>✂️</button>
    </>
  )
}

export default InputPostUrl
