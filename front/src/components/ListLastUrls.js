import React from 'react'

const ListLastUrls = ({ lastUrls }) => {
  if (lastUrls.length > 0) {
    return (
      <div className='flex flex-col'>
        <div className='pt-10 pl-10 pb-2 text-gray-100 text-2xl text-opacity-50'>Derniers ✂️</div>
        {lastUrls.map((url, index) => {
          return <a className="text-gray-200 italic" href={url} key={index} target='_blank' rel='noreferrer'>{url}</a>
        })}
      </div>
    )
  }
  return (<p className='pt-10 pl-10 text-gray-100 text-opacity-50 text-2xl'>Aucun ✂️</p>)
}

export default ListLastUrls
