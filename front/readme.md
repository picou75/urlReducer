## Sujet (côté front)
https://urlreducer.netlify.app/

Application qui permet de rentrer une url afin de la raccourcir.
Possibilité de voir les dernières url raccourcies et les utiliser en cliquant directement dessus

Utilisation de nano-react-app (Parcel) et tailwincss : volonté de produire une application légère
