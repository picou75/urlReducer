import Hashids from 'hashids'
import config from '../config/index.js'

const hashids = new Hashids(config.hash.salt, parseInt(config.hash.minLength))

function reduction (id) {
  return hashids.encode(id)
}

function identification (hash) {
  return hashids.decode(hash)[0]
}

export default {
  reduction,
  identification
}
