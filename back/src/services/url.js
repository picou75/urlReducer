import converter from './converter.js'
import urlQueries from '../db/urlQueries.js'

const reduceUrl = async (url) => {
  let urlId = await urlQueries.searchUrl(url)
  if (urlId === 0) {
    urlId = await urlQueries.createUrl(url)
  }
  return urlId ? converter.reduction(urlId) : null
}

const redirectionUrl = async (urlCode) => {
  const urlId = converter.identification(urlCode)
  const url = await urlQueries.findUrlById(urlId)
  return url || null
}

export default {
  reduceUrl,
  redirectionUrl
}
