import bodyParser from 'body-parser'
import cors from 'cors'
import routes from '../api/index.js'
import path from 'path'
const __dirname = path.resolve('src/views')

export default ({ app }) => {
  app.set('view engine', 'pug')
  app.set('views', path.join(__dirname))

  app.get('/status', (req, res) => {
    res.status(200).end()
  })

  app.use(cors())
  app.use(bodyParser.json())

  app.use('', routes())

  app.use((req, res, next) => {
    const err = new Error('Not Found')
    err.status = 404
    next(err)
  })

  app.use((err, req, res, next) => {
    if (err.name === 'RedirectionError') {
      return res.render('error', { type: err.name, message: err.message })
    }
    return next(err)
  })

  app.use((err, req, res, next) => {
    res.status(err.status || 500)
    res.json({
      errors: {
        message: err.message
      }
    })
  })
}
