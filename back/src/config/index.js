import dotenv from 'dotenv'
dotenv.config()

export default {
  port: process.env.PORT,
  api: {
    shortUrl: process.env.API_SHORT_URL
  },
  logs: {
    level: process.env.LOG_LEVEL
  },
  db: {
    user: process.env.DB_USER,
    database: process.env.DB_DATABASE,
    port: process.env.DB_PORT,
    host: process.env.DB_HOST,
    password: process.env.DB_PASSWORD
  },
  hash: {
    salt: process.env.HASH_SALT,
    minLength: process.env.HASH_MINLENGTH
  }
}
