import express from 'express'

import config from './config/index.js'
import logger from './loaders/logger.js'
import expressLoaders from './loaders/express.js'

async function startServer () {
  const app = express()
  expressLoaders({ app })

  app.listen(config.port, () => {
    logger.info(`Listening on ${config.port}...`)
  }).on('error', err => {
    logger.error(err)
    process.exit(1)
  })
}

startServer()
