import pg from 'pg'
import config from '../config/index.js'
import logger from '../loaders/logger.js'
const Pool = pg.Pool

const db = new Pool({
  user: config.db.user,
  host: config.db.host,
  database: config.db.database,
  password: config.db.password,
  port: config.db.port,
  ssl: { rejectUnauthorized: false }
})

const searchUrl = async (url) => {
  return await db.query('SELECT id FROM urls WHERE url = $1', [url])
    .then(res => {
      return res.rows[0].id
    }).catch(err => {
      logger.error('searchUrl :' + err)
      return 0
    })
}

const findUrlById = async (id) => {
  return await db.query('SELECT url FROM urls WHERE id = $1', [id])
    .then(res => {
      if (res.rowCount === 0) {
        return null
      }
      return res.rows[0].url
    })
    .catch(err => {
      logger.error('findUrlById :' + err)
      return null
    })
}

const createUrl = async (url) => {
  return await db.query('INSERT INTO urls (url) VALUES ($1) RETURNING id', [url])
    .then(res => {
      logger.info('createUrl :' + res.rows[0].id)
      return res.rows[0].id
    })
    .catch(err => {
      logger.error('createUrl :' + err)
      return null
    })
}

const deleteUrlById = async (id) => {
  return db.query('DELETE FROM urls WHERE id = $1', [id])
    .then(res => {
      logger.info('deleteUrl')
      return true
    })
    .catch(err => {
      logger.error('deleteUrlById :' + err)
      return null
    })
}

export default {
  searchUrl,
  findUrlById,
  createUrl,
  deleteUrlById
}
