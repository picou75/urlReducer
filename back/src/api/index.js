import express from 'express'
import url from './routes/url.js'

const { Router } = express

export default () => {
  const app = Router()
  url(app)
  return app
}
