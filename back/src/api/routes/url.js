import express from 'express'
import validUrl from 'valid-url'

import config from '../../config/index.js'
import urlService from '../../services/url.js'

const { Router } = express
const route = Router()

export default (app) => {
  app.use('/url', route)

  route.post('/reducer', async (req, res, next) => {
    const url = req.body.url
    if (!validUrl.isUri(url)) {
      return next(new Error('Impossible de réduire une url invalide'))
    }
    const urlCode = await urlService.reduceUrl(url)
    if (!urlCode) {
      next(new Error('Impossible de réduire cette url'))
    } else {
      const shortUrl = config.api.shortUrl + urlCode
      return res.json({ shortUrl }).status(200)
    }
  })

  route.get('/:urlCode', async (req, res, next) => {
    const urlCode = req.params.urlCode
    const urlOrigin = await urlService.redirectionUrl(urlCode)
    if (!urlOrigin) {
      const err = new Error('Impossible de rediriger cette url')
      err.name = 'RedirectionError'
      next(err)
    } else {
      return res.redirect(urlOrigin)
    }
  })
}
