import converter from '../../src/services/converter.js'

describe('reduction function', () => {
  it('should always return the same reduction for the id 3', () => {
    const hash = converter.reduction(3)
    const hash2 = converter.reduction(3)
    expect(hash).toBe(hash2)
  })

  it('should return the same reduction for the id 3', () => {
    const hash = converter.reduction(3)
    const hash2 = converter.reduction(3)
    expect(hash).toBe(hash2)
  })

  it('should return two differents reductions for id 3 and id 2', () => {
    const hash = converter.reduction(3)
    const hash2 = converter.reduction(2)
    expect(hash === hash2).toBe(false)
  })
})

describe('identification function', () => {
  it('should return 3', () => {
    const hash = converter.reduction(3)
    const id = converter.identification(hash)
    expect(id).toBe(3)
  })
})
