import express from 'express'
import url from '../../src/api/routes/url.js'

const app = express()
app.use('/states', url)

describe('reduction function', () => {
  it('should always return the same reduction for the id 3', () => {
    expect(1).toBe(1)
  })
})
