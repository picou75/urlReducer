API REST qui permet de raccourcir une url et de rediriger vers l'url originale.
lien de l'API: https://urlreducerapi.herokuapp.com/

Commandes du projet:
- npm run dev (environnement de developpement)
- npm start (environnement de déploiement)
- npm run lint:fix (pour fix les problèmes de lint)

## Endpoints
Clairement pas la meilleure url pour un service de réduction d'url mais il s'agit d'une url d'hébergement gratuit. 
(Heroku)
- POST https://urlreducerapi.herokuapp.com/url/reducer (body: { url : "https://www.mon-url-a-raccourcir.com" })
- GET https://urlreducerapi.herokuapp.com/url/:urlCode (query param : urlCode)

## Structure du projet
Projet composé:
- d'un dossier src/ qui contient tout le code relatif à l'application
    - server.js : point d'accès de l'application
    - config/ : relatif à la configuration de l'application (ici les variables d'environnement)
    - loaders/ : relatif à la mise en place de l'application (ici un service de log, et surtout la configuration express)
    - api/ : relatif aux controlleurs des routes de l'API
    - db/ : relatif à la couche data access de l'application
- d'un dossier tests/ qui contient les tests relatifs à chaque sous-dossiers de src/*

## notes
git subtree push --prefix back heroku master
